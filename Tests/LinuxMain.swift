import XCTest

import ToolsAFLTests

var tests = [XCTestCaseEntry]()
tests += ToolsAFLTests.allTests()
XCTMain(tests)
