//
//  DefaultCollection.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit

@IBDesignable
open class DefaultCollection: UICollectionView {
  
  
  // ---------------------------------------------------------------------
  // MARK: Constants
  // ---------------------------------------------------------------------
  
  
  open var layout: UICollectionViewLayout =  UICollectionViewFlowLayout()
  
  
  // ---------------------------------------------------------------------
  // MARK: Variables FlowLayout
  // ---------------------------------------------------------------------
  
  
  @IBInspectable open var rows: CGFloat = 1
  @IBInspectable open var cols: CGFloat = 1
  @IBInspectable open var minimumInterCol: CGFloat = 0
  @IBInspectable open var minimumInterRow: CGFloat = 0
  
  @IBInspectable open var bottomInset: CGFloat {
    get { return collectionInsets.bottom }
    set { collectionInsets.bottom = newValue }
  }
  @IBInspectable open var leftInset: CGFloat {
    get { return collectionInsets.left }
    set { collectionInsets.left = newValue }
  }
  @IBInspectable open var rightInset: CGFloat {
    get { return collectionInsets.right }
    set { collectionInsets.right = newValue }
  }
  @IBInspectable open var topInset: CGFloat {
    get { return collectionInsets.top }
    set { collectionInsets.top = newValue }
  }
  
  open var collectionInsets: UIEdgeInsets = .zero {
    didSet {
      if collectionInsets.left > 0 && minimumInterCol == 0 {
        minimumInterCol = collectionInsets.left
      }
      
      if collectionInsets.top > 0 && minimumInterRow == 0 {
        minimumInterRow = collectionInsets.top
      }
    }
  }
  open var width: CGFloat!
  open var height: CGFloat!
  open var onRefresh: ( () -> () )?
  
  
  // -----------------------------------------
  // MARK: Constructor
  // -----------------------------------------
  
  
  
  public init() {
    super.init(frame: .zero, collectionViewLayout: layout)
    _setup()
  }
  
  required public init?(coder: NSCoder) {
    super.init(coder: coder)
    _setup()
  }
  
  open override func draw(_ rect: CGRect) {
    super.draw(rect)
    updateFlowLayout(rect)
  }
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  
  private func _setup(){
  }
  
  open func addWithParent(view: UIView) {
    translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(self)
    NSLayoutConstraint.activate([
      topAnchor.constraint(equalTo: view.topAnchor),
      bottomAnchor.constraint(equalTo: view.bottomAnchor),
      leadingAnchor.constraint(equalTo: view.leadingAnchor),
      trailingAnchor.constraint(equalTo: view.trailingAnchor)
    ])
  }
  
  
  open func updateFlowLayout(_ frame: CGRect) {
    if let layout = self.layout as? UICollectionViewFlowLayout {
      if layout.scrollDirection == .vertical {
        width = calcWidth(width: frame.width, direction: layout.scrollDirection)
        height = height == nil ? width : height
      } else {
        height = calcWidth(width: frame.height, direction: layout.scrollDirection)
        width = width == nil ? height : width
      }
    }
    self.setupLayout()
  }
  
  open func setupRefreshCtrlCollection() {
    refreshControl = UIRefreshControl()
    refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
  }
  
  private func setupLayout() {
    if let layout = self.layout as? UICollectionViewFlowLayout {
      layout.minimumInteritemSpacing = minimumInterCol
      layout.minimumLineSpacing = minimumInterRow
      layout.estimatedItemSize = CGSize(
        width: width,
        height: height
      )
      layout.itemSize = CGSize(
        width: width,
        height: height
      )
      layout.sectionInset = .init(
        top: collectionInsets.top,
        left: collectionInsets.left,
        bottom: collectionInsets.bottom,
        right: collectionInsets.right
      )
    }
    self.setCollectionViewLayout(layout, animated: false)
  }
  
  @objc open func refresh( ) {
    onRefresh?()
  }
  
  open func calcWidth(
    width: CGFloat,
    direction: UICollectionView.ScrollDirection = .vertical
  ) -> CGFloat {
    
    if direction == .vertical {
      let cols = CGFloat(cols + 1.0 )
      return  ((width - (cols * (minimumInterCol ) )) / CGFloat(self.cols)) - 1
    }
    
    let rows = CGFloat(rows + 1.0 )
    return  (width - (rows * (minimumInterRow ) )) / CGFloat(self.rows)
  }
}



public protocol IDefaultCollection {
  
  
  // ---------------------------------------------------------------------
  // MARK: IDefaultCollection
  // ---------------------------------------------------------------------
  
  associatedtype ModelItem
  
  func numberOfRowsInSection(_ section: Int) -> Int
  func numberOfSections() -> Int
  func getItemAt(_ indexPath: IndexPath) -> ModelItem?
  func didSelectItemAt(_ indexPath: IndexPath) -> Void
}
