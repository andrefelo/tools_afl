//
//  DefaultCollectionController.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit

open class DefaultCollectionController<T>: CollectionController<T>, UICollectionViewDelegateFlowLayout {
  
  // ---------------------------------------------------------------------
  // MARK: Views
  // ---------------------------------------------------------------------
  
  
  open var searchController: UISearchController?
  
  
  // ---------------------------------------------------------------------
  // MARK: Variables
  // ---------------------------------------------------------------------
  
  
  open var rows: CGFloat = 1
  open var cols: CGFloat = 1
  open var minimumInterCol: CGFloat = 0
  open var minimumInterRow: CGFloat = 0
  open var width: CGFloat!
  open var height: CGFloat!
  open var onRefresh: ( () -> () )?
  open var collectionInsets: UIEdgeInsets = .zero {
    didSet {
      if collectionInsets.left > 0 && minimumInterCol == 0 {
        minimumInterCol = collectionInsets.left
      }
    }
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Life cycle
  // ---------------------------------------------------------------------
  
  
  open override func viewDidLoad() {
    super.viewDidLoad()
    _setup()
  }

  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  

  private func _setup(){
    updateFlowLayout()
  }
  
  
  open func setupRefreshCtrlCollection() {
    collectionView.refreshControl = UIRefreshControl()
    collectionView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
  }
  
  
  open func setupSearchController(){
    searchController = UISearchController(searchResultsController: nil)
    searchController?.obscuresBackgroundDuringPresentation = false
    definesPresentationContext = true
    navigationItem.searchController = searchController
  }
  
  
  open func updateFlowLayout() {
    if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
      if layout.scrollDirection == .vertical {
        width = calcWidth(width: view.frame.width, direction: layout.scrollDirection)
        height = height == nil ? width : height
      } else {
        height = calcWidth(width: view.frame.height, direction: layout.scrollDirection)
        width = width == nil ? height : width
      }
    }
  }
  
  
  open func calcWidth(
    width: CGFloat,
    direction: UICollectionView.ScrollDirection = .vertical
  ) -> CGFloat {
    
    if direction == .vertical {
      let cols = CGFloat(cols + 1.0 )
      return  ((width - (cols * (minimumInterCol ) )) / CGFloat(self.cols)) - 1
    }
    
    let rows = CGFloat(rows + 1.0 )
    return  (width - (rows * (minimumInterRow ) )) / CGFloat(self.rows)
  }
  
  
  @objc open func refresh( ) {
    onRefresh?()
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: UICollectionViewDelegateFlowLayout
  // ---------------------------------------------------------------------
  
  
  open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: width, height: height)
  }
  
  
  open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return .init(
      top: collectionInsets.top,
      left: collectionInsets.left,
      bottom: collectionInsets.bottom,
      right: collectionInsets.right
    )
  }
  
  
  open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return minimumInterRow
  }
  
  
  open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return minimumInterCol
  }
  
  
  open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
    return .zero
  }
  
  
  open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
    return .zero
  }
}
