//
//  CircleAnimator.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit

open class CircleAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  
  
  // ---------------------------------------------------------------------
  // MARK: variables for the Animator
  // ---------------------------------------------------------------------
  
  
  open var mode: TransitionMode = .present
  open var circle = UIView()
  open var circleColor: UIColor
  open var circleOrigin: CGPoint = CGPoint.zero
  open var animationDuration: TimeInterval
  
  
  // ---------------------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------------------
  
  
  public init(view: UIView, color: UIColor, duration: TimeInterval) {
    var point = view.center
    if let nn = view.superview?.convert(view.frame, to: nil) {
      point = CGPoint.init(x: nn.midX, y: nn.midY)
    }
    self.circleOrigin = point
    self.circleColor = color
    self.animationDuration = duration
  }
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  // MARK:- functions for the circleAnimator
  open func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return animationDuration
  }
  
  /// returns the frame the circle is supposed to occupy while transitioning
  open func frameForCircle(center: CGPoint, size: CGSize, start: CGPoint) -> CGRect{
    let lengthX = fmax(start.x, size.width - start.x)
    let lengthY = fmax(start.y, size.height - start.y)
    let offset = sqrt(lengthX * lengthX + lengthY * lengthY) * 2
    let size = CGSize(width: offset, height: offset)
    return CGRect(origin: CGPoint.zero, size: size)
  }
  
  /// the function for transitioning between the viewControllers
  open func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView
    
    if mode == .present {
      guard let presentedView = transitionContext.view(forKey: .to) else { return }
      let viewCenter = presentedView.center
      
      
      let viewSize = presentedView.frame.size
      
      /// set a circle in place of the selectedButton
      circle = UIView(frame: frameForCircle(center: viewCenter, size: viewSize, start: circleOrigin))
      circle.layer.cornerRadius = circle.frame.width / 2.0
      circle.center = circleOrigin
      
      /// make the circle small so that we can animate
      circle.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
      circle.backgroundColor = circleColor
      
      
      /// container view is where the action transition takes place
      containerView.addSubview(circle)
      
      /// presentedView is the view we will transition to
      presentedView.center = circleOrigin
      presentedView.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
      presentedView.backgroundColor = circleColor
      
      /// add the presentedView to container so that we can animate it too
      containerView.addSubview(presentedView)
      
      /// animate the circle and the presentedView and remove it after completion
      UIView.animate(withDuration: animationDuration, delay: 0, options: .curveEaseOut, animations: {
        self.circle.transform = CGAffineTransform.identity
        presentedView.transform = CGAffineTransform.identity
        presentedView.center = viewCenter
      }, completion: { (finished) in
        self.circle.removeFromSuperview()
        transitionContext.completeTransition(true)
      })
    } else {
      guard let returnController = transitionContext.view(forKey: .from) else { return }
      let viewCenter = returnController.center
      let viewSize = returnController.frame.size
      
      /// set a circle in place of the selectedButton
      circle.frame = frameForCircle(center: viewCenter, size: viewSize, start: circleOrigin)
      
      circle.layer.cornerRadius = circle.frame.width / 2.0
      circle.center = circleOrigin
      circle.backgroundColor = circleColor
      
      /// container view is where the action transition takes place
      containerView.addSubview(circle)
      
      /// animate the circle and the returningController and remove it after completion
      UIView.animate(withDuration: animationDuration, delay: 0, options: .curveEaseOut, animations: {
        self.circle.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        returnController.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        returnController.center = self.circleOrigin
        returnController.alpha = 0
        
      }, completion: {finished in
        returnController.removeFromSuperview()
        self.circle.removeFromSuperview()
        transitionContext.completeTransition(true)
      })
    }
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Enums
  // ---------------------------------------------------------------------
  
  
  public enum TransitionMode {
    case present, dismiss
  }
}

