//
//  UIControllerCircularAnimation.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit

open class UIControllerCircularAnimation: UIViewController {
  
  
  // ---------------------------------------------------------------------
  // MARK: variables
  // ---------------------------------------------------------------------
  
  
  open var circleAnimator: CircleAnimator?
  open var withFirstCircle: Bool = false
  open var selectedButton = UIView()
  open var bgCircle: UIColor = .white
  open var duration: Double = 0.4
  
  
  // ---------------------------------------------------------------------
  // MARK: Constructor
  // ---------------------------------------------------------------------
  
  
  public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }

  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Life cycle
  // ---------------------------------------------------------------------
  
  
  open override func viewDidLoad() {
    super.viewDidLoad()
//    setup()
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  private func setup() {
    transitioningDelegate = self
  }
  
}

extension UIControllerCircularAnimation: UIViewControllerTransitioningDelegate {
  
  
  // ---------------------------------------------------------------------
  // MARK: UIViewControllerTrasitioningDelegates
  // ---------------------------------------------------------------------
  
  
  public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    circleAnimator = CircleAnimator(view: selectedButton, color: bgCircle, duration: duration)
    circleAnimator?.mode = .dismiss
    return circleAnimator
  }
  
  public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    circleAnimator = CircleAnimator(
      view: selectedButton,
      color: bgCircle,
      duration: duration
    )
    circleAnimator?.mode = .present
    return circleAnimator
  }
}
