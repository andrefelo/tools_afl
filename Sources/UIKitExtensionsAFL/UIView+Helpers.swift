//
//  UIView+Helpers.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit

public extension UIView {
	
  
  @IBInspectable var cornerRadius:CGFloat {
    get { return layer.cornerRadius }
    set {
      clipsToBounds = true
      layer.cornerRadius = newValue
    }
  }
  
	
  func addBlur(style: UIBlurEffect.Style ) {
    let blurEffect = UIBlurEffect(style: style)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    blurEffectView.frame = self.bounds
    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    self.addSubview(blurEffectView)
  }
	
	
	func addShadow(
		radius: CGFloat = 25,
		shadowRadius: CGFloat = 10,
		shadowOpacity: Float = 0.2,
		shadowColor: UIColor = UIColor(white: 0.3, alpha: 0.8),
		offset: CGSize = CGSize(width: 0, height: 8)
	) {
		layer.shadowColor = shadowColor.cgColor
		layer.shadowOpacity = shadowOpacity
		layer.shadowOffset =  offset
		layer.shadowRadius = shadowRadius
		layer.shouldRasterize = true
		layer.masksToBounds = false
		layer.cornerRadius = radius
		layer.rasterizationScale = UIScreen.main.scale
	}
}



public extension UIView {
	func makeSnapshot() -> UIImage? {
		if #available(iOS 10.0, *) {
			let renderer = UIGraphicsImageRenderer(size: bounds.size)
			return renderer.image { _ in drawHierarchy(in: bounds, afterScreenUpdates: true) }
		} else {
			return layer.makeSnapshot()
		}
	}
}



public extension UIImage {
	convenience init?(snapshotOf view: UIView) {
		guard let image = view.makeSnapshot(), let cgImage = image.cgImage else { return nil }
		self.init(cgImage: cgImage, scale: image.scale, orientation: image.imageOrientation)
	}
}


public extension CALayer {
	func makeSnapshot() -> UIImage? {
		let scale = UIScreen.main.scale
		UIGraphicsBeginImageContextWithOptions(frame.size, false, scale)
		defer { UIGraphicsEndImageContext() }
		guard let context = UIGraphicsGetCurrentContext() else { return nil }
		render(in: context)
		let screenshot = UIGraphicsGetImageFromCurrentImageContext()
		return screenshot
	}
}

