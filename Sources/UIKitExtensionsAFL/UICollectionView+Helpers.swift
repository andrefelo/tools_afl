//
//  UICollectionView+Helpers.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit

public extension UICollectionView {
  
  func dequeueCell<T:UICollectionViewCell>(_ identifier: String? = nil, indexPath: IndexPath) -> T {
    let identifier = identifier ?? String(describing: T.self)
    let cell = self.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? T
    switch cell {
      case .some(let unwrapped):  return unwrapped
      default: fatalError("Unable to dequeue" + T.self.description())
    }
  }
  
  
  func register<T:UICollectionViewCell>(_ type: T.Type, identifier: String? = nil) {
    let identifier = identifier ?? String(describing: T.self)
    register(type, forCellWithReuseIdentifier: identifier)
  }
  
  
  func registerFromNib<T:UICollectionViewCell>(_ type: T.Type, identifier: String? = nil) {
    let identifier = identifier ?? String(describing: T.self)
    register(
      UINib.init(nibName: String(describing: T.self), bundle: nil),
      forCellWithReuseIdentifier: identifier
    )
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: UICollectionReusableView
  // ---------------------------------------------------------------------

  
  func dequeueSupplementary<T:UICollectionReusableView>(_ identifier: String? = nil, indexPath: IndexPath, kind: String) -> T {
    let identifier = identifier ?? String(describing: T.self)
    let view = self.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: identifier, for: indexPath) as? T
    switch view {
      case .some(let unwrapped):  return unwrapped
      default: fatalError("Unable to dequeue" + T.self.description())
    }
  }
  
  
  func register<T:UICollectionReusableView>(_ type: T.Type, identifier: String? = nil, kind: String) {
    let identifier = identifier ?? String(describing: T.self)
    register(type, forSupplementaryViewOfKind: kind, withReuseIdentifier: identifier)
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Refresh
  // ---------------------------------------------------------------------

  
  
  func beginRefreshing() {
    // Make sure that a refresh control to be shown was actually set on the view
    // controller and the it is not already animating. Otherwise there's nothing
    // to refresh.
    guard let refreshControl = refreshControl, !refreshControl.isRefreshing else {
      return
    }
    
    // Start the refresh animation
    refreshControl.beginRefreshing()
    
    // Make the refresh control send action to all targets as if a user executed
    // a pull to refresh manually
    refreshControl.sendActions(for: .valueChanged)
    
    // Apply some offset so that the refresh control can actually be seen
    let contentOffset = CGPoint(x: 0, y: -refreshControl.frame.height)
    setContentOffset(contentOffset, animated: true)
  }
  
  func endRefreshing() {
    refreshControl?.endRefreshing()
  }
}
