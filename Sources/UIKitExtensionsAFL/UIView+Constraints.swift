//
//  File.swift
//  
//
//  Created by Andres Lozano on 27/05/21.
//

import UIKit

public extension UIView {

//	func handleConstrain(safeAreaConstraints:[SafeAreaConstraints]) -> ItemConstrains {
//		translatesAutoresizingMaskIntoConstraints = false
//		return getConstraintWidthSafeArea(safeAreaConstraints)
//	}
//
//
//	@discardableResult
//	func anchor(
//		top: NSLayoutYAxisAnchor? = nil,
//		leading: NSLayoutXAxisAnchor? = nil,
//		bottom: NSLayoutYAxisAnchor? = nil,
//		trailing: NSLayoutXAxisAnchor? = nil,
//		centerX: NSLayoutXAxisAnchor? = nil,
//		centerY: NSLayoutYAxisAnchor? = nil,
//		padding: UIEdgeInsets = .zero,
//		size: CGSize = .zero
//	) -> AnchoredConstraints {
//		translatesAutoresizingMaskIntoConstraints = false
//		var anchoredConstraints = AnchoredConstraints()
//
//		if let top = top {
//			anchoredConstraints.top = topAnchor.constraint(equalTo: top, constant: padding.top)
//		}
//
//		if let leading = leading {
//			anchoredConstraints.leading = leadingAnchor.constraint(equalTo: leading, constant: padding.left)
//		}
//
//		if let bottom = bottom {
//			anchoredConstraints.bottom = bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom)
//		}
//
//		if let trailing = trailing {
//			anchoredConstraints.trailing = trailingAnchor.constraint(equalTo: trailing, constant: -padding.right)
//		}
//
//		if size.width != 0 {
//			anchoredConstraints.width = widthAnchor.constraint(equalToConstant: size.width)
//    }
//
//		if size.height != 0 {
//			anchoredConstraints.height = heightAnchor.constraint(equalToConstant: size.height)
//    }
//
//		[
//			anchoredConstraints.top,
//			anchoredConstraints.leading,
//			anchoredConstraints.bottom,
//			anchoredConstraints.trailing,
//			anchoredConstraints.width,
//			anchoredConstraints.height
//		].forEach{ $0?.isActive = true }
//
//		return anchoredConstraints
//	}
//
//
//  func getConstraintWidthSafeArea(_ safeAreaConstraints: [SafeAreaConstraints]) -> ItemConstrains {
//    var result = ItemConstrains()
//
//    for item in safeAreaConstraints {
//      switch item {
//        case .top:
//          result.top = layoutguide?.topAnchor
//        case .bottom:
//          result.bottom = layoutguide?.bottomAnchor
//        case .leading:
//          result.leading = layoutguide?.leadingAnchor
//        case .trainling:
//          result.trailing = layoutguide?.trailingAnchor
//        case .centerX:
//          result.centerX = layoutguide?.centerXAnchor
//        case .centerY:
//          result.centerY = layoutguide?.centerYAnchor
//        case .width:
//          result.width = layoutguide?.widthAnchor
//        case .heigth:
//          result.heigth = layoutguide?.heightAnchor
//      }
//    }
//    return result
//  }
//
//
//	func fillSuperview(
//		safeAreaConstraints:[SafeAreaConstraints] = [],
//		padding: UIEdgeInsets = .zero,
//		with safeArea: Bool = true
//	) -> Void {
//		let auxConstraints = handleConstrain(safeAreaConstraints: safeAreaConstraints)
//
//    if let anchor =  (safeArea) ?  auxConstraints.top : superview?.topAnchor  {
//      topAnchor.constraint(
//        equalTo: anchor,
//        constant: padding.top
//      ).isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.bottom : superview?.bottomAnchor  {
//      bottomAnchor.constraint(
//        equalTo: anchor,
//        constant: -padding.bottom
//      ).isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.leading : superview?.leadingAnchor  {
//      leadingAnchor.constraint(
//        equalTo: anchor,
//        constant: padding.left
//      ).isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.trailing : superview?.trailingAnchor  {
//      trailingAnchor.constraint(
//        equalTo: anchor,
//        constant: -padding.right
//      ).isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.width : superview?.widthAnchor  {
//      let constraint = widthAnchor.constraint(equalTo: anchor)
//      constraint.priority = .defaultLow
//      constraint.isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.heigth : superview?.heightAnchor  {
//      let constraint = heightAnchor.constraint(equalTo: anchor)
//      constraint.priority = .defaultLow
//      constraint.isActive = true
//    }
//	}
//
//
//	func centerInSuperview(size: CGSize = .zero) -> Void {
//		removeAllConstraints()
//    translatesAutoresizingMaskIntoConstraints = false
//		if let anchor = superview?.centerXAnchor {
//			centerXAnchor.constraint(equalTo: anchor).isActive = true
//		}
//
//		if let anchor = superview?.centerYAnchor {
//			centerYAnchor.constraint(equalTo: anchor).isActive = true
//		}
//
//		if size.width != 0 {
//			widthAnchor.constraint(equalToConstant: size.width).isActive = true
//		}
//
//		if size.height != 0 {
//			heightAnchor.constraint(equalToConstant: size.height).isActive = true
//		}
//	}
//
//
//  func centerInSuperview(
//    safeAreaConstraints:[SafeAreaConstraints] = [],
//    padding: UIEdgeInsets = .zero,
//    with safeArea: Bool = true
//  ){
//    removeAllConstraints()
//
//    let auxConstraints = handleConstrain(
//      safeAreaConstraints: safeAreaConstraints.isEmpty ? [.bottom, .top, .leading, .trainling] : safeAreaConstraints
//    )
//
//    if let anchor =  (safeArea) ?  auxConstraints.centerY : superview?.centerYAnchor  {
//      let constraint = centerYAnchor.constraint(equalTo: anchor)
//      constraint.priority = .required
//      constraint.isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.top : superview?.topAnchor  {
//      let constraint = topAnchor.constraint(greaterThanOrEqualTo: anchor, constant: padding.top)
//      constraint.priority = .defaultLow
//      constraint.isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.bottom : superview?.bottomAnchor  {
//      let constraint = bottomAnchor.constraint(greaterThanOrEqualTo: anchor, constant: -padding.bottom)
//      constraint.priority = .defaultLow
//      constraint.isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.leading : superview?.leadingAnchor  {
//      leadingAnchor.constraint(
//        equalTo: anchor,
//        constant: padding.left
//      ).isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.trailing : superview?.trailingAnchor  {
//      trailingAnchor.constraint(
//        equalTo: anchor,
//        constant: -padding.right
//      ).isActive = true
//    }
//  }
//
//
//	func centerInSuperviewExpanableY(
//		safeAreaConstraints:[SafeAreaConstraints] = [],
//		padding: UIEdgeInsets = .zero,
//		with safeArea: Bool = true
//	) -> Void {
//
//		removeAllConstraints()
//
//    let auxConstraints = handleConstrain(
//      safeAreaConstraints: safeAreaConstraints.isEmpty ? [.bottom, .top, .leading, .trainling] : safeAreaConstraints
//    )
//
//    if let anchor =  (safeArea) ?  auxConstraints.centerY : superview?.centerYAnchor  {
//      let constraint = centerYAnchor.constraint(equalTo: anchor)
//      constraint.priority = .defaultLow
//      constraint.isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.top : superview?.topAnchor  {
//      let constraint = topAnchor.constraint(greaterThanOrEqualTo: anchor, constant: padding.top)
//      constraint.priority = .required
//      constraint.isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.bottom : superview?.bottomAnchor  {
//      let constraint = bottomAnchor.constraint(greaterThanOrEqualTo: anchor, constant: -padding.bottom)
//      constraint.priority = .required
//      constraint.isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.leading : superview?.leadingAnchor  {
//      leadingAnchor.constraint(
//        equalTo: anchor,
//        constant: padding.left
//      ).isActive = true
//    }
//
//    if let anchor =  (safeArea) ?  auxConstraints.trailing : superview?.trailingAnchor  {
//      trailingAnchor.constraint(
//        equalTo: anchor,
//        constant: -padding.right
//      ).isActive = true
//    }
//	}
//
//
//	func removeAllConstraints() {
//		let constraints = self.superview?.constraints.filter{
//			$0.firstItem as? UIView == self || $0.secondItem as? UIView == self
//		} ?? []
//
//		self.superview?.removeConstraints(constraints)
//		self.removeConstraints(self.constraints)
//	}
//
//
//	func removeMyConstraintsFromSuperView() {
//		if let items = superview?.constraints.filter({ ($0.firstItem as? UIView) == self }) {
//			superview?.removeConstraints(items)
//		}
//	}
//
//
//	fileprivate var layoutguide: UILayoutGuide? {
//		if #available(iOS 11.0, *) {
//			return  superview?.safeAreaLayoutGuide
//		} else {
//			return superview?.layoutMarginsGuide
//		}
//	}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  


	struct AnchoredConstraints {
		var top, leading, bottom, trailing, width, height: NSLayoutConstraint?
	}
  
  struct ItemConstrains {
    var top: NSLayoutYAxisAnchor?
    var bottom: NSLayoutYAxisAnchor?
    var leading: NSLayoutXAxisAnchor?
    var trailing: NSLayoutXAxisAnchor?
    var centerX: NSLayoutXAxisAnchor?
    var centerY: NSLayoutYAxisAnchor?
    var width:NSLayoutDimension?
    var heigth: NSLayoutDimension?
  }


	enum SafeAreaConstraints {
		case top, bottom, leading, trainling, centerY, centerX, width, heigth
	}
  
  
  
  
  
  
  @discardableResult
  func anchor(top: NSLayoutYAxisAnchor? = nil, leading: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, trailing: NSLayoutXAxisAnchor? = nil, padding: UIEdgeInsets = .zero, size: CGSize = .zero) -> AnchoredConstraints {
    
    translatesAutoresizingMaskIntoConstraints = false
    var anchoredConstraints = AnchoredConstraints()
    
    if let top = top {
      anchoredConstraints.top = topAnchor.constraint(equalTo: top, constant: padding.top)
    }
    
    if let leading = leading {
      anchoredConstraints.leading = leadingAnchor.constraint(equalTo: leading, constant: padding.left)
    }
    
    if let bottom = bottom {
      anchoredConstraints.bottom = bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom)
    }
    
    if let trailing = trailing {
      anchoredConstraints.trailing = trailingAnchor.constraint(equalTo: trailing, constant: -padding.right)
    }
    
    if size.width != 0 {
      anchoredConstraints.width = widthAnchor.constraint(equalToConstant: size.width)
    }
    
    if size.height != 0 {
      anchoredConstraints.height = heightAnchor.constraint(equalToConstant: size.height)
    }
    
    [anchoredConstraints.top, anchoredConstraints.leading, anchoredConstraints.bottom, anchoredConstraints.trailing, anchoredConstraints.width, anchoredConstraints.height].forEach{ $0?.isActive = true }
    
    return anchoredConstraints
  }
  
  
  fileprivate func getConstraintWidthSafeArea(_ safeAreaConstraints:[SafeAreaConstraints]) -> (top: NSLayoutYAxisAnchor?, bottom: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, trailing: NSLayoutXAxisAnchor?, centerX: NSLayoutXAxisAnchor?, centerY: NSLayoutYAxisAnchor?, width:NSLayoutDimension?, heigth: NSLayoutDimension? )  {
    
    var safeTop:NSLayoutYAxisAnchor?
    var safeBottom:NSLayoutYAxisAnchor?
    var safeLeading:NSLayoutXAxisAnchor?
    var safeTrailing:NSLayoutXAxisAnchor?
    var safeCenterX:NSLayoutXAxisAnchor?
    var safeCenterY:NSLayoutYAxisAnchor?
    var safeHeigth:NSLayoutDimension?
    var safeWidth:NSLayoutDimension?
    
    for item in safeAreaConstraints {
      switch item {
        case .top:
          if #available(iOS 11.0, *) {
            safeTop = superview?.safeAreaLayoutGuide.topAnchor
          } else {
            safeTop = superview?.layoutMarginsGuide.topAnchor
          }
        case .bottom:
          if #available(iOS 11.0, *) {
            safeBottom = superview?.safeAreaLayoutGuide.bottomAnchor
          } else {
            safeBottom = superview?.layoutMarginsGuide.bottomAnchor
          }
        case .leading:
          if #available(iOS 11.0, *) {
            safeLeading = superview?.safeAreaLayoutGuide.leadingAnchor
          } else {
            safeLeading = superview?.layoutMarginsGuide.leadingAnchor
          }
        case .trainling:
          if #available(iOS 11.0, *) {
            safeTrailing = superview?.safeAreaLayoutGuide.trailingAnchor
          } else {
            safeTrailing = superview?.layoutMarginsGuide.trailingAnchor
          }
        case .centerX:
          if #available(iOS 11.0, *) {
            safeCenterX = superview?.safeAreaLayoutGuide.centerXAnchor
          } else {
            safeCenterX = superview?.layoutMarginsGuide.centerXAnchor
          }
        case .centerY:
          if #available(iOS 11.0, *) {
            safeCenterY = superview?.safeAreaLayoutGuide.centerYAnchor
          } else {
            safeCenterY = superview?.layoutMarginsGuide.centerYAnchor
          }
        case .width:
          if #available(iOS 11.0, *) {
            safeWidth = superview?.safeAreaLayoutGuide.widthAnchor
          } else {
            safeWidth = superview?.layoutMarginsGuide.widthAnchor
          }
        case .heigth:
          if #available(iOS 11.0, *) {
            safeHeigth = superview?.safeAreaLayoutGuide.heightAnchor
          } else {
            safeHeigth = superview?.layoutMarginsGuide.heightAnchor
          }
      }
    }
    return (top: safeTop, bottom: safeBottom, leading: safeLeading, trailing: safeTrailing, centerX: safeCenterX,
            centerY: safeCenterY, width: safeWidth, heigth: safeHeigth)
  }
  
  func fillSuperview(safeAreaConstraints:[SafeAreaConstraints] = [], padding: UIEdgeInsets = .zero) {
    translatesAutoresizingMaskIntoConstraints = false
    
    let auxConstraints = getConstraintWidthSafeArea(safeAreaConstraints)
    
    
    if let safeTop = auxConstraints.top {
      topAnchor.constraint(equalTo: safeTop, constant: padding.top).isActive = true
    }else if let superviewTopAnchor = superview?.topAnchor {
      topAnchor.constraint(equalTo: superviewTopAnchor, constant: padding.top).isActive = true
    }
    
    if let safeBottom = auxConstraints.bottom {
      bottomAnchor.constraint(equalTo: safeBottom, constant: -padding.bottom).isActive = true
    } else if let superviewBottomAnchor = superview?.bottomAnchor {
      bottomAnchor.constraint(equalTo: superviewBottomAnchor, constant: -padding.bottom).isActive = true
    }
    
    if let safeLeading = auxConstraints.leading {
      leadingAnchor.constraint(equalTo: safeLeading, constant: padding.left).isActive = true
    } else if let superviewLeadingAnchor = superview?.leadingAnchor {
      leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: padding.left).isActive = true
    }
    
    if let safeTrailing = auxConstraints.trailing {
      trailingAnchor.constraint(equalTo: safeTrailing, constant: -padding.right).isActive = true
    } else if let superviewTrailingAnchor = superview?.trailingAnchor {
      trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: -padding.right).isActive = true
    }
    
    if let safeWith = auxConstraints.width {
      let constraint = widthAnchor.constraint(equalTo: safeWith)
      constraint.priority = .defaultLow
      constraint.isActive = true
    } else if let superWidthAnchor = superview?.widthAnchor {
      let constraint = widthAnchor.constraint(equalTo: superWidthAnchor)
      constraint.priority = .defaultLow
      constraint.isActive = true
    }
    
    if let safeHeigth = auxConstraints.heigth {
      let constraint = heightAnchor.constraint(equalTo: safeHeigth)
      constraint.priority = .defaultLow
      constraint.isActive = true
    } else if let superHeigthAnchor = superview?.heightAnchor {
      let constraint = heightAnchor.constraint(equalTo: superHeigthAnchor)
      constraint.priority = .defaultLow
      constraint.isActive = true
    }
  }
  
  func centerInSuperview(size: CGSize = .zero) {
    removeAllConstraints()
    translatesAutoresizingMaskIntoConstraints = false
    if let superviewCenterXAnchor = superview?.centerXAnchor {
      centerXAnchor.constraint(equalTo: superviewCenterXAnchor).isActive = true
    }
    
    if let superviewCenterYAnchor = superview?.centerYAnchor {
      centerYAnchor.constraint(equalTo: superviewCenterYAnchor).isActive = true
    }
    
    if size.width != 0 {
      widthAnchor.constraint(equalToConstant: size.width).isActive = true
    }
    
    if size.height != 0 {
      heightAnchor.constraint(equalToConstant: size.height).isActive = true
    }
  }
  
  
  
  
  
  func centerInSuperview(safeAreaConstraints:[SafeAreaConstraints] = [], padding: UIEdgeInsets = .zero){
    removeAllConstraints()
    translatesAutoresizingMaskIntoConstraints = false
    
    let aux = safeAreaConstraints.isEmpty ? [.bottom, .top, .leading, .trainling] : safeAreaConstraints
    let auxConstraints = getConstraintWidthSafeArea(aux)
    
    if let safeCenterY = auxConstraints.centerY {
      let constraint = centerYAnchor.constraint(equalTo: safeCenterY)
      constraint.priority = .required
      constraint.isActive = true
    } else if let superviewCenterYAnchor = superview?.centerYAnchor {
      let constraint = centerYAnchor.constraint(equalTo: superviewCenterYAnchor)
      constraint.priority = .required
      constraint.isActive = true
    }
    
    if let safeTop = auxConstraints.top {
      let constraint = topAnchor.constraint(greaterThanOrEqualTo: safeTop, constant: padding.top)
      constraint.priority = .defaultLow
      constraint.isActive = true
    }else if let superviewTopAnchor = superview?.topAnchor {
      let constraint = topAnchor.constraint(greaterThanOrEqualTo: superviewTopAnchor, constant: padding.top)
      constraint.priority = .defaultLow
      constraint.isActive = true
    }
    
    if let safeBottom = auxConstraints.bottom {
      let constraint = bottomAnchor.constraint(greaterThanOrEqualTo: safeBottom, constant: -padding.bottom)
      constraint.priority = .defaultLow
      constraint.isActive = true
    } else if let superviewBottomAnchor = superview?.bottomAnchor {
      let constraint = bottomAnchor.constraint(greaterThanOrEqualTo: superviewBottomAnchor, constant: -padding.bottom)
      constraint.priority = .defaultLow
      constraint.isActive = true
    }
    
    if let safeLeading = auxConstraints.leading {
      leadingAnchor.constraint(equalTo: safeLeading, constant: padding.left).isActive = true
    } else if let superviewLeadingAnchor = superview?.leadingAnchor {
      leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: padding.left).isActive = true
    }
    
    if let safeTrailing = auxConstraints.trailing {
      trailingAnchor.constraint(equalTo: safeTrailing, constant: -padding.right).isActive = true
    } else if let superviewTrailingAnchor = superview?.trailingAnchor {
      trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: -padding.right).isActive = true
    }
  }
  
  
  
  
  func centerInSuperviewExpanable(safeAreaConstraints:[SafeAreaConstraints] = [], padding: UIEdgeInsets = .zero){
    removeAllConstraints()
    translatesAutoresizingMaskIntoConstraints = false
    
    let aux = safeAreaConstraints.isEmpty ? [.bottom, .top, .leading, .trainling] : safeAreaConstraints
    let auxConstraints = getConstraintWidthSafeArea(aux)
    
    if let safeCenterY = auxConstraints.centerY {
      let constraint = centerYAnchor.constraint(equalTo: safeCenterY)
      constraint.priority = .defaultLow
      constraint.isActive = true
    } else if let superviewCenterYAnchor = superview?.centerYAnchor {
      let constraint = centerYAnchor.constraint(equalTo: superviewCenterYAnchor, constant: padding.left)
      constraint.priority = .defaultLow
      constraint.isActive = true
    }
    
    if let safeTop = auxConstraints.top {
      let constraint = topAnchor.constraint(greaterThanOrEqualTo: safeTop, constant: padding.top)
      constraint.priority = .required
      constraint.isActive = true
    }else if let superviewTopAnchor = superview?.topAnchor {
      let constraint = topAnchor.constraint(greaterThanOrEqualTo: superviewTopAnchor, constant: padding.top)
      constraint.priority = .required
      constraint.isActive = true
    }
    
    if let safeBottom = auxConstraints.bottom {
      let constraint = bottomAnchor.constraint(greaterThanOrEqualTo: safeBottom, constant: -padding.bottom)
      constraint.priority = .required
      constraint.isActive = true
    } else if let superviewBottomAnchor = superview?.bottomAnchor {
      let constraint = bottomAnchor.constraint(greaterThanOrEqualTo: superviewBottomAnchor, constant: -padding.bottom)
      constraint.priority = .required
      constraint.isActive = true
    }
    
    if let safeLeading = auxConstraints.leading {
      leadingAnchor.constraint(equalTo: safeLeading, constant: padding.left).isActive = true
    } else if let superviewLeadingAnchor = superview?.leadingAnchor {
      leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: padding.left).isActive = true
    }
    
    if let safeTrailing = auxConstraints.trailing {
      trailingAnchor.constraint(equalTo: safeTrailing, constant: -padding.right).isActive = true
    } else if let superviewTrailingAnchor = superview?.trailingAnchor {
      trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: -padding.right).isActive = true
    }
  }
  
  func removeAllConstraints() {
    let constraints = self.superview?.constraints.filter{
      $0.firstItem as? UIView == self || $0.secondItem as? UIView == self
    } ?? []
    
    self.superview?.removeConstraints(constraints)
    self.removeConstraints(self.constraints)
  }
  
  func removeMyConstraintsFromSuperView() {
    if let items = superview?.constraints.filter({ ($0.firstItem as? UIView) == self }) {
      superview?.removeConstraints(items)
    }
  }
}
