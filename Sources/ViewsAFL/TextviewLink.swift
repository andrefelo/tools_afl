//
//  TextviewLink.swift
//
//  Copyright (c) Andres F. Lozano
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit


open class TextviewLink: UITextView {
  
  
  // ---------------------------------------------
  // MARK: - Atributes
  // ---------------------------------------------
  
  
  public var fontSize = UIFont.boldSystemFont(ofSize: 14)
  public var customLink = "customLink"
  public var firstColor:UIColor = .black
  public var secondColor:UIColor = .blue
  public weak var textviewLinkDelegate:TextviewLinkDelegate?
  public override var text: String?{
    didSet {
      if let newValue = self.text, !newValue.isEmpty{
        setupFeatures(newValue)
      }
    }
  }
  
  
  // ---------------------------------------------
  // MARK: - Constructor
  // ---------------------------------------------
  
  
  public override init(frame: CGRect, textContainer: NSTextContainer?) {
    super.init(frame: frame, textContainer: textContainer)
    setup()
  }
  
  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  
  // ---------------------------------------------
  // MARK: - Helpers funcs
  // ---------------------------------------------
  
  
  private func setup() {
    translatesAutoresizingMaskIntoConstraints = false
    sizeToFit()
    isScrollEnabled = false
    isEditable = false
    isSelectable = true
    dataDetectorTypes = [.link]
    delegate = self
  }
  
  fileprivate func setupFeatures(_ text: String)  {
    guard let index = text.firstIndex(of: "#"),
          case let start = text.index(after: index),
          let end = text[start...].firstIndex(of: "#") else {
      return
    }
    linkTextAttributes = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font: fontSize,
      .foregroundColor: secondColor,
      .underlineColor: secondColor
    ]
    
    let substring = String(text[start..<end])
    let finalText = text.replacingOccurrences(of: "#", with: "")
    
    let feature1: [NSAttributedString.Key : Any] = [
      .font: fontSize,
      .foregroundColor: firstColor
    ]
    let atrs = NSMutableAttributedString(string: finalText,attributes: feature1)
    super.text = finalText
    atrs.addAttribute(.link, value: customLink, range: (finalText as NSString).range(of: substring))
    attributedText = atrs
  }
  
  fileprivate func substring(with nsrange: NSRange) -> Substring? {
    guard let range = Range(nsrange, in: self.text!) else { return nil }
    return self.text![range]
  }
  
}

extension TextviewLink: UITextViewDelegate{
  
  
  // ---------------------------------------------
  // MARK: - UITextViewDelegate
  // ---------------------------------------------
  
  
  public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
    if URL.path == customLink {
      var str:String? = nil
      if let sbs = substring(with: characterRange){
        str = String(sbs)
      }
      textviewLinkDelegate?.onTouch(textView, link: customLink, text: str)
      return false
    }
    return true
  }
}

public protocol TextviewLinkDelegate: AnyObject {
  func onTouch(_ item: UITextView, link:String, text:String?)
}
